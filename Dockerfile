FROM erlang:alpine

LABEL authors="Alex Leyn <alexleyn@me.com>"

WORKDIR /usr/

RUN apk --no-cache add git make
RUN git clone https://github.com/erlang/rebar3.git && \
	cd rebar3 && \
	./bootstrap && \
	./rebar3 local install
ENV PATH=/root/.cache/rebar3/bin:$PATH

RUN git clone https://github.com/Antibiotic/websocket_chat.git
RUN make compile -C /usr/websocket_chat/

CMD make start -C /usr/websocket_chat/